import React from "react";
import normatives from "../../Data/products";

class Products extends React.Component {
  state = {
    allProducts: normatives,
  };

  renderNormatives(normatives) {
    return (
      <table className="table table-bordered">
        <thead>
          <tr>
            <th scope="col">#</th>
            <th scope="col">Name</th>
            <th scope="col">Amount</th>
            <th scope="col">Unit</th>
          </tr>
        </thead>
        <tbody>
          {normatives &&
            normatives.map((norm, idx) => {
              return (
                <tr key={idx}>
                  <td>{idx}</td>
                  <td>{norm.name}</td>
                  <td>{norm.amount}</td>
                  <td>{norm.unit}</td>
                </tr>
              );
            })}
        </tbody>
      </table>
    );
  }

  showHideNormatives(prod) {
    const val =
      typeof this.state[`${prod.id}_showIngredients`] == undefined
        ? true
        : !this.state[`${prod.id}_showIngredients`];

    this.setState({
      [`${prod.id}_showIngredients`]: val,
    });
  }

  render() {
    return (
      <div className="mt-5">
        <table className="table table-fixed table-bordered">
          <thead>
            <tr>
              <th style={{ width: "5%" }}>#</th>
              <th style={{ width: "15%" }}>Name</th>
              <th style={{ width: "20%" }}>Description</th>
              <th style={{ width: "8%" }}>Unit</th>
              <th style={{ width: "30%" }}>Ingredients</th>
            </tr>
          </thead>
          <tbody>
            {this.state.allProducts.map((prod, idx) => {
              const showIngredients = this.state[`${prod.id}_showIngredients`];

              return (
                <tr key={idx}>
                  <td>{prod.id}</td>
                  <td>{prod.name}</td>
                  <td>{prod.description}</td>
                  <td>{prod.unit}</td>
                  <td>
                    <span
                      className="c-pointer"
                      onClick={() => this.showHideNormatives(prod)}
                    >
                      {showIngredients ? "Hide" : "Show"} normatives
                    </span>

                    {showIngredients && this.renderNormatives(prod.ingredients)}
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}

export default Products;
