import React from "react";

class DisplayOrder extends React.Component {
  state = {
    isOpened: false,
  };

  onPlaceOrder() {
    this.setState({
      isOpened: false,
    });

    this.props.closeModal();
  }

  render() {
    return (
      <div>
        <div
          className="modal bd-example-modal-lg"
          style={{ display: this.props.openModal ? "block" : "none" }}
          role="dialog"
          aria-labelledby="myLargeModalLabel"
          aria-hidden="true"
        >
          <div className="modal-dialog modal-lg">
            <div className="modal-content">
              <div className="modal-header">
                Order of ingredients you need to make.
              </div>

              <div className="modal-body">
                <table className="table table-fixed table-bordered">
                  <thead>
                    <tr>
                      <th style={{ width: "5%" }}>#</th>
                      <th style={{ width: "25%" }}>Name</th>
                      <th style={{ width: "25%" }}>Unit</th>
                      <th style={{ width: "25%" }}>Amount</th>
                    </tr>
                  </thead>
                  <tbody>
                    {Object.entries(this.props.ingredients).map(
                      (entry, idx) => {
                        const ing = entry[1];
                        return (
                          <tr key={idx}>
                            <td>{idx}</td>
                            <td>{ing.name}</td>
                            <td>{ing.unit}</td>
                            <td>{ing.amount}</td>
                          </tr>
                        );
                      }
                    )}
                  </tbody>
                </table>
              </div>

              <div className="modal-footer">
                <button
                  onClick={(e) => this.onPlaceOrder(e)}
                  className="btn btn-primary"
                >
                  Place the order
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default DisplayOrder;
