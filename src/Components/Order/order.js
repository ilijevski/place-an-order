import React from "react";
import products from "../../Data/products";
import DisplayOrder from "../DisplayOrder/displayOrder";

class Order extends React.Component {
  state = {
    ordered: [],
    products: products,
    selectedProd: { name: "--" },
    ingredientsToOrder: null,
  };

  selectProd(e) {
    let chosenProduct = this.state.products.find((p) => p.id == e.target.value);

    if (chosenProduct) {
      this.setState({
        selectedProd: chosenProduct,
      });
    }
  }

  updateSelected(e) {
    this.setState({
      selectedProd: {
        ...this.state.selectedProd,
        amount: e.target.value,
      },
    });
  }

  onAddItem(e) {
    this.setState({
      ordered: [...this.state.ordered, this.state.selectedProd],
      selectedProd: {
        amount: "",
        unit: "",
        description: "",
        name: "--",
        id: "--",
      },
    });
  }

  generateOrder(ordered) {
    const allIngredients = {};

    ordered.forEach((order) => {
      order.ingredients.forEach((i) => {
        if (!allIngredients[i.name]) {
          allIngredients[i.name] = {
            name: i.name,
            amount: parseFloat(i.amount) * parseFloat(order.amount),
            unit: i.unit,
          };
        } else {
          allIngredients[i.name].amount =
            allIngredients[i.name].amount +
            parseFloat(i.amount) * parseFloat(order.amount);
        }
      });
    });

    if (allIngredients) {
      this.setState({
        ingredientsToOrder: allIngredients,
      });
    }
  }
  closeModal() {
    this.setState({ ingredientsToOrder: null, ordered: [] });
  }

  render() {
    return (
      <div className="mt-5">
        {this.state.ingredientsToOrder && (
          <DisplayOrder
            closeModal={() => this.closeModal()}
            openModal={this.state.ingredientsToOrder != null}
            ingredients={this.state.ingredientsToOrder}
          ></DisplayOrder>
        )}

        <button
          disabled={this.state.ordered.length == 0}
          onClick={() => this.generateOrder(this.state.ordered)}
          className="mb-2 btn btn-primary"
        >
          Generate Order
        </button>

        <table className="table table-fixed table-bordered">
          <thead>
            <tr>
              <th style={{ width: "5%" }}>#</th>
              <th style={{ width: "25%" }}>Name</th>
              <th style={{ width: "20%" }}>Description</th>
              <th style={{ width: "15%" }}>Unit</th>
              <th style={{ width: "15%" }}>Amount</th>
              <th style={{ width: "13%" }}>Actions</th>
            </tr>
          </thead>

          <tbody>
            {this.state.ordered.map((prod, idx) => {
              return (
                <tr key={idx}>
                  <td>{prod.id}</td>
                  <td>{prod.name}</td>
                  <td>{prod.description}</td>
                  <td>{prod.unit}</td>
                  <td>{prod.amount}</td>
                  <td></td>
                </tr>
              );
            })}

            {/* Add new order tr */}
            <tr>
              <td>{this.state.ordered.length + 1}</td>
              <td>
                <select
                  value={this.state.selectedProd.id}
                  onChange={(e) => {
                    this.selectProd(e);
                  }}
                  className="form-control"
                >
                  <option value="--">Select product</option>
                  {this.state.products.map((prod) => (
                    <option key={prod.id} value={prod.id}>
                      {prod.name}
                    </option>
                  ))}
                </select>
              </td>
              <td>{this.state.selectedProd.description}</td>
              <td>{this.state.selectedProd.unit}</td>
              <td>
                <input
                  value={this.state.selectedProd.amount || ""}
                  name="amount"
                  onChange={(e) => {
                    this.updateSelected(e);
                  }}
                  className="form-control"
                  type="text"
                />
              </td>
              <td>
                <button
                  disabled={this.state.selectedProd.name == "--"}
                  onClick={(e) => {
                    this.onAddItem(e);
                  }}
                  className="btn btn-success"
                >
                  Add Item
                </button>
              </td>
            </tr>
          </tbody>
        </table>
      </div>
    );
  }
}

export default Order;
