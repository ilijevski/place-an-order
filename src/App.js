import "bootstrap/dist/css/bootstrap.min.css";
import React from 'react';
import { BrowserRouter as Router, NavLink, Route, Switch } from 'react-router-dom';
import './App.css';
import Order from './Components/Order/order';
import Products from './Components/ProductsPage/productsPage';

class App extends React.Component {


  render() {
    return (
      <div>
        <Router>
          <div className="navbar navbar-dark container">
            <ul className="nav">
              <li className="nav-item"><NavLink className="nav-link" activeClassName="active-menu" to="/products">Products</NavLink></li>
              <li className="nav-item"><NavLink className="nav-link" activeClassName="active-menu" to="/order">Order</NavLink></li>
            </ul>
          </div>

          <div className="container">
            <Switch>
              <Route path="/products" exact component={Products}></Route>
              <Route path="/order" exact component={Order}></Route>
            </Switch>
          </div>
        </Router>
      </div>
    );
  }
}

export default App;
